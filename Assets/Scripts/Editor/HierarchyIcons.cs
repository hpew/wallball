﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class HierarchyIcons
{

	static readonly Texture2D Cyan;
	static readonly Texture2D Orange;
	static readonly Texture2D Yellow;
	static readonly Texture2D Gray;

	static readonly List<int> CyanMarked = new List<int>();
	static readonly List<int> OrangeMarked = new List<int>();
	static readonly List<int> YellowMarked = new List<int>();
	static readonly List<int> GrayMarked = new List<int>();

	static HierarchyIcons()
	{
		Cyan = AssetDatabase.LoadAssetAtPath("Assets/Scripts/Editor/Icons/blue.png", typeof(Texture2D)) as Texture2D;
		Orange = AssetDatabase.LoadAssetAtPath("Assets/Scripts/Editor/Icons/orange.png", typeof(Texture2D)) as Texture2D;
		Yellow = AssetDatabase.LoadAssetAtPath("Assets/Scripts/Editor/Icons/yellow.png", typeof(Texture2D)) as Texture2D;
		Gray = AssetDatabase.LoadAssetAtPath("Assets/Scripts/Editor/Icons/grey.png", typeof(Texture2D)) as Texture2D;

		EditorApplication.hierarchyChanged += Update;
		EditorApplication.hierarchyWindowItemOnGUI += DrawHierarchyItemIcon;
	}

	static void Update()
	{
		CyanMarked.Clear();
		OrangeMarked.Clear();
		YellowMarked.Clear();
		GrayMarked.Clear();

		GameObject[] go = Object.FindObjectsOfType(typeof(GameObject), true) as GameObject[];

		foreach (GameObject g in go)
		{
			if (g == null) continue;

			int instanceId = g.GetInstanceID();

			if (g.tag == "orange")
				OrangeMarked.Add(instanceId);
			else if (g.tag == "blue")
				CyanMarked.Add(instanceId);
			else if (g.tag == "yellow")
				YellowMarked.Add(instanceId);
			else if (g.tag == "grey")
				GrayMarked.Add(instanceId);
		}
	}

	static void DrawHierarchyItemIcon(int instanceId, Rect selectionRect)
	{
		Rect r = new Rect(selectionRect);
		r.x += r.width - 25;
		r.width = 18;

		if (CyanMarked.Contains(instanceId))
			GUI.Label(r, Cyan);
		if (OrangeMarked.Contains(instanceId))
			GUI.Label(r, Orange);
		if (YellowMarked.Contains(instanceId))
			GUI.Label(r, Yellow);
		if (GrayMarked.Contains(instanceId))
			GUI.Label(r, Gray);
	}
}
