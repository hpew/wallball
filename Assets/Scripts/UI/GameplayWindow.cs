﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameplayWindow : Window
{
    private GameoverWindow gameoverWindow;
    private PauseWindow pauseWindow;

    public override void Init(UIManager UImanager)
    {
        base.Init(UImanager);

        gameoverWindow = UImanager.Get<GameoverWindow>();
        pauseWindow = UImanager.Get<PauseWindow>();
        //gameoverWindow.OnClose += (x) => Close();
    }
    protected override void SelfOpen(Action<Window> callback)
    {
        this.gameObject.SetActive(true);
        ChangeCurrentWindow(this);

        gameObject.transform.position = new Vector3(-Screen.width/2, gameObject.transform.position.y, gameObject.transform.position.z);
        LeanTween.moveX(gameObject, Screen.width/2, 0.5f)
            .setEase(LeanTweenType.easeInOutBack)
            .setOnComplete(() =>
            {
                callback?.Invoke(this);
            });


    }

    protected override void SelfClose(Action<Window> callback)
    {
        this.gameObject.SetActive(false);
        callback?.Invoke(this);
    }

    public void GameoverWindowOpen()
    {
        gameoverWindow.Open(() => CurrentWindow = gameoverWindow);
    }
    public void GameoverWindowClose()
    {
        gameoverWindow.Close(() => CurrentWindow = this);
    }

    public void PauseWindowOpen()
    {
        pauseWindow.Open(() => CurrentWindow = pauseWindow);
    }
    public void PauseWindowClose()
    {
        pauseWindow.Close(() => CurrentWindow = this);
    }

    protected override void EscapeClose(Action<Window> callback)
    {

    }
}
