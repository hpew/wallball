﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HPEW.UI
{
    public class UIConteiner : MonoBehaviour
    {
        public GameObject menuPanel;
        public GameObject gameoverPanel;

        public Text coinsText;
        public Text scoreText;
        public Text recordScoreText;
        public Text lastScoreText;
        public Text magnetCountText;
        public Text autogameCountText;
        public Text coinsbagCountText;

        public Button magnetButton;
        public Button autogameButton;
        public Button coinsbagButton;
    }
}
