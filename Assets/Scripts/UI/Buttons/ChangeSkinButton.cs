﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniRx;
using UnityEditor;

public class ChangeSkinButton : Button
{
    public HPEW.Input.TypeSwipe typeSwipe;
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        MessageBroker.Default
            .Publish(HPEW.ShellSystem.Customization.Message.Create(typeSwipe));
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(ChangeSkinButton))]
public class MenuButtonEditor : UnityEditor.UI.ButtonEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        ChangeSkinButton targetButton = (ChangeSkinButton)target;

        targetButton.typeSwipe = (HPEW.Input.TypeSwipe)EditorGUILayout.EnumPopup("Change Side", targetButton.typeSwipe);

        // Show default inspector property editor
    }
}
#endif
