﻿using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using Random = UnityEngine.Random;
using HPEW.Math;

namespace HPEW.Interactive
{
    [RequireComponent(typeof(TextMeshPro))]
    public class HPPopup : MonoBehaviour
    {
        private TextMeshPro _tmp;
        private Vector3 direction = Vector3.up;

        [SerializeField, Range(0, 90f)]
        private float angleDeviations = 0f;

        [SerializeField, Range(0f, 10f)]
        private float speed = 5f;

        private void Awake()
        {
            _tmp = GetComponent<TextMeshPro>();

            var angle = Random.Range(-angleDeviations, angleDeviations);
            direction = Geometry.RotateVector(direction, angle, Axis.Z);
        }

        public void Setup(string message)
        {
            _tmp.SetText(message);
        }

        public void Update()
        {
            transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);
            transform.position += direction * speed * Time.deltaTime;
        }

        public static HPPopup Create(Transform prefab, Vector3 position, string message)
        {
            var transform = Instantiate(prefab, position, Quaternion.identity);
            var popup = transform.GetComponent<HPPopup>();

            popup.Setup(message);

            return popup;
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
