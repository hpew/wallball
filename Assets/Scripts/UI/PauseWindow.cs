﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;

public class PauseWindow : Window
{
    public override void Init(UIManager UImanager)
    {
        base.Init(UImanager);
    }
    protected override void SelfOpen(Action<Window> callback)
    {
        this.gameObject.SetActive(true);
        Time.timeScale = 0;
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, -Screen.height / 2, gameObject.transform.position.z);
        LeanTween.moveY(gameObject, Screen.height / 2, 0.5f)
            .setIgnoreTimeScale(true)
            .setEase(LeanTweenType.easeOutBack)
            .setOnComplete(() =>
            {
                callback?.Invoke(this);
            });

    }

    protected override void SelfClose(Action<Window> callback)
    {
        Time.timeScale = 1;
        LeanTween.moveY(gameObject, -Screen.height / 2, 0.5f)
            .setIgnoreTimeScale(true)
            .setEase(LeanTweenType.easeInOutBack)
            .setOnComplete(() =>
            {
                this.gameObject.SetActive(false);
                callback?.Invoke(this);
            });
    }

    protected override void EscapeClose(Action<Window> callback)
    {

    }
}
