﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using HPEW.Input;
using HPEW.Systemic;
using HPEW.Pool;

namespace HPEW.ShellSystem
{
    [RequireComponent(typeof(ShellMotionController))]
    public class Shell : MonoBehaviour
    {
        [Inject] ShellMotionController _motionController;
        [Inject] Settings _settings;

        public event Action OnEndGame = delegate { };

        private void Awake()
        {
            
        }

        public void StartShell()
        {
            Observable.Timer(TimeSpan.FromSeconds(_settings.ShotTime))
                .Subscribe((Action<long>)delegate
                {
                    _motionController.StartMoving(() => OnEndGame());
                });
        }

        public void StartContinue()
        {
            Observable.Timer(TimeSpan.FromSeconds(_settings.ShotTime))
                .Subscribe((Action<long>)delegate
                {
                    _motionController.ContinueMoving(() => OnEndGame());
                });
        }
    }
}
