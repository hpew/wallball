﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using Zenject;

namespace HPEW.ShellSystem
{
    [System.Serializable]
    public struct CustomizeParameters
    {
        public string nameID;
        public Material material;
        public List<GameObject> skinParts;
        public List<GameObject> skinRagdollParts;
    }
    public class Customization : MonoBehaviour
    {
        [Inject] PlayerAnimationController _animator;
        public Material originalMaterials;
        public List<CustomizeParameters> customizes;

        private List<string> customizesName;

        private Renderer _renderer;
        private Renderer _ragdollRenderer;

        public string CurrentSkin { get; private set; } = "";

        private void Awake()
        {
            _renderer = GetComponentInChildren<SkinnedMeshRenderer>();
            _ragdollRenderer = GetComponentInChildren<RagdollController>(true)?.GetComponentInChildren<SkinnedMeshRenderer>(true);
        }

        private void Start()
        {
            customizesName = new List<string>();
            customizesName.Add("");
            foreach (var item in customizes)
            {
                customizesName.Add(item.nameID);
            }
            _renderer.material = originalMaterials;
            _ragdollRenderer.material = originalMaterials;
            MessageBroker.Default
       .Receive<Message>()
       .Subscribe(msg =>
       {
           int index = customizesName.FindIndex(x => x.Equals(CurrentSkin));
           if (msg.swipe == Input.TypeSwipe.Left)
           {
               if (index <= 0)
                   index = customizesName.Count;
               ChangeSkin(customizesName[index - 1]);
           }
           else if (msg.swipe == Input.TypeSwipe.Right)
           {
               if (index >= customizesName.Count - 1)
                   index = -1;
               ChangeSkin(customizesName[index + 1]);
           }
           _animator.Play_Dress();
       });
        }

        private void Update()
        {
            //if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha6))
            //{
            //    ChangeSkin("Duck");
            //}
            //if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha5))
            //{
            //    ChangeSkin("Mag");
            //}
            //if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha4))
            //{
            //    ChangeSkin("Clown");
            //}
            //if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha3))
            //{
            //    ChangeSkin("Atom");
            //}
            //if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha2))
            //{
            //    ChangeSkin("Batman");
            //}
            //if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha1))
            //{
            //    ChangeSkin("");
            //}
        }

        public void ChangeSkin(string name)
        {
            CurrentSkin = name;
            bool isChange = false;
            foreach (var skin in customizes)
            {
                if (skin.nameID == name)
                {
                    _renderer.material = skin.material;
                    _ragdollRenderer.material = skin.material;
                    foreach (var element in skin.skinParts)
                    {
                        element.SetActive(true);
                    }
                    foreach (var element in skin.skinRagdollParts)
                    {
                        element.SetActive(true);
                    }
                    isChange = true;
                }
                else
                {
                    foreach (var element in skin.skinParts)
                    {
                        element.SetActive(false);
                    }
                    foreach (var element in skin.skinRagdollParts)
                    {
                        element.SetActive(false);
                    }
                }
            }
            if (!isChange)
            {
                _renderer.material = originalMaterials;
                _ragdollRenderer.material = originalMaterials;
            }
        }

        public class Message
        {
            public Input.TypeSwipe swipe { get; private set; }
            public Message(Input.TypeSwipe swipe)
            {
                this.swipe = swipe;
            }

            public static Message Create(Input.TypeSwipe swipe)
            {
                return new Message(swipe);
            }
        }
    }
}
