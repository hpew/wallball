﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace HPEW.ShellSystem
{
    public class Ghost : MonoBehaviour
    {
        GameObject recordGhost;
        GameObject lastGhost;

        [Inject] Score score;

        private void Awake()
        {
            recordGhost = transform.GetChild(0).gameObject;
            lastGhost = transform.GetChild(1).gameObject;
        }

        public void EnableRecordGhost(Vector3 pos)
        {
            float value = score.RecordScore * 10;
            if (value >= pos.z - 50 && value <= pos.z + 50)
            {
                recordGhost.SetActive(true);
                recordGhost.transform.position = new Vector3(-8, 5, value);
            }
        }

        public void DisableRecordGhost()
        {
            if (recordGhost.activeSelf)
            {
                recordGhost.SetActive(false);
                recordGhost.transform.position = Vector3.zero;
            }
        }

        public void EnableLastGhost(Vector3 pos)
        {
            float value = score.LastScore * 10;
            if (value >= pos.z - 50 && value <= pos.z + 50)
            {
                lastGhost.SetActive(true);
                lastGhost.transform.position = new Vector3(-8, 5, value);
            }
        }

        public void DisableLastGhost()
        {
            if (lastGhost.activeSelf)
            {
                lastGhost.SetActive(false);
                lastGhost.transform.position = Vector3.zero;
            }
        }
    }
}
