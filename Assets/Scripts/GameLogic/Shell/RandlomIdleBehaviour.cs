﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandlomIdleBehaviour : StateMachineBehaviour
{
    public string paramName = "IdleState";
    public int[] stateAray = { 0, 1, 2 };
    [Header("")]
    public float minChangeTime = 1f;
    public float maxChangeTime = 3f;
    public float speed = 10;

    private float timer = 1;
    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called before OnStateExit is called on any state inside this state machine
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMachineEnter is called when entering a state machine via its Entry Node
    override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    {
        if (timer <= 0)
        {
            RandomAnimation(animator);
            timer = Random.Range(minChangeTime, maxChangeTime);
        }
        else
        {
            timer -= Time.deltaTime * speed;
            animator.SetInteger(paramName, 0);
            //Debug.Log("TimeLeft: " + timer);
        }
    }

    void RandomAnimation(Animator animator)
    {
        if (stateAray.Length <= 0)
        {
            animator.SetInteger(paramName, 0);
        }
        else
        {
            int index = Random.Range(0, stateAray.Length);
            animator.SetInteger(paramName, stateAray[index]);
        }
    }

    // OnStateMachineExit is called when exiting a state machine via its Exit Node
    //override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    //{
    //    
    //}
}
