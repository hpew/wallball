﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using HPEW.Input;
using HPEW.ChunkSystem;
using UniRx;
using HPEW.Systemic;

namespace HPEW.Interactive
{
    public class InteractiveZone : MonoBehaviour
    {
        [Inject] InputSystem _inputSystem;
        [Inject] GameManager manager;
        [Inject] BusterSystem busterSpawner;
        [Inject] Assets _assets;
        [Inject] ShellSystem.Shell _shell;
        public Queue<Let> Lets { get; private set; }

        void Awake()
        {
            Lets = new Queue<Let>();

            _inputSystem.OnTouch += type =>
            {
                if (type == Input.TouchType.OneTouch)
                    TouchHandler();
            };

            _inputSystem.OnSwipe += SwipeHandler;

            manager.OnRestart += ResetQueue;
            busterSpawner.OnBusterInteract += SkipLet;
            manager.OnResurract += () =>
            {
                foreach (var buster in _assets.BusterPrefabs)
                {
                    if (buster is AutogameBuster)
                    {
                        Observable.FromCoroutine(_=> ResurractDelay(buster, Lets.Count))
                            .Subscribe();
                        break;
                    }
                }
            };
        }

        IEnumerator ResurractDelay(Buster buster, int count)
        {
            int amount = count;
            while (amount != 0)
            {
                Lets.Peek().AutoInteract(buster);
                Lets.Dequeue();
                amount--;
                if (amount >= 3)
                    yield return null;
                else
                    yield return new WaitForSeconds(1f);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            var let = other.GetComponent<Let>();

            if (let != null && let.isInteractive && !Lets.Contains(let))
            {
                Lets.Enqueue(let);
                //Debug.Log("Enqueue " + let.name);
            }
        }

        private void TouchHandler()
        {
            if (Lets.Count != 0 && !manager.IsDead)
            {
                var let = Lets.Peek();

                //Debug.Log("Touch " + let.name + " " + let.transform.position);
                if (let is TouchLet || let is DestructibleLet || let is FallingLet)
                {
                    let.Interact();

                    if (!let.isInteractive)
                        Lets.Dequeue();
                }
            }
        }

        private void SwipeHandler(TypeSwipe type)
        {
            if (Lets.Count != 0 && Lets.Peek() is SwipeLet sLet && !manager.IsDead)
            {
                sLet.Interact(type);

                if (!sLet.isInteractive)
                    Lets.Dequeue();
            }
        }

        private void ResetQueue()
        {
            Lets.Clear();
        }

        private int skippedLet = 0;
        private void SkipLet(object data, System.Action action)
        {
            if (data is AutogameBuster)
            {
                var dispose = Observable.FromMicroCoroutine(_ => Delay(data, action))
                .Subscribe();

                _shell.OnEndGame += () =>
                {
                    dispose?.Dispose();
                    action?.Invoke();
                };
            }
        }

        IEnumerator Delay(object data, System.Action action)
        {
            while (Lets.Count == 0)
            {
                yield return null;
            }
            Lets.Peek().AutoInteract(data);
            Lets.Dequeue();
            skippedLet++;
            //Debug.Log("skippedLet " + skippedLet);
            if (skippedLet == (data as AutogameBuster).Duration)
            {
                Debug.Log("EndSprint");
                skippedLet = 0;
                action?.Invoke();
            }
        }
    }
}