﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

public class FallingBehaviour : StateMachineBehaviour
{
    [SerializeField, Range(0f, 100f)]
    private float height = 10f;
    [SerializeField, Range(0f, 100f)]
    private float speed = 10f;

    private float startHeight;
    private bool isFalling = false;

    [Inject] GameManager manager;
    [Inject] PlayerAnimationController playerAnimation;
    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //}

    IEnumerator Falling(Animator animator)
    {
        animator.transform.GetChild(1).gameObject.SetActive(true);
        animator.transform.GetChild(0).gameObject.SetActive(true);
        Observable.Timer(System.TimeSpan.FromSeconds(0.01f))
            .Subscribe(_ =>
            {
                animator.SetBool("Ressuraction", false);
                isFalling = false;
            });
        while (animator.transform.localPosition.y != startHeight)
        {
            animator.transform.localPosition = Vector3.MoveTowards(animator.transform.localPosition, new Vector3(
            animator.transform.localPosition.x, startHeight, animator.transform.localPosition.z), Time.deltaTime * speed);
            yield return null;
        }
    }

    // OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called before OnStateExit is called on any state inside this state machine
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{    
    //}

    // OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMachineEnter is called when entering a state machine via its Entry Node
    override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    {
        if (isFalling) return;
        FindObjectOfType<PauseButton>().interactable = false;
        isFalling = true;
        startHeight = animator.transform.localPosition.y;
        animator.transform.localPosition = new Vector3(
            animator.transform.localPosition.x, height, animator.transform.localPosition.z);
        Observable.FromMicroCoroutine(_ => Falling(animator))
            .Subscribe();
    }

    // OnStateMachineExit is called when exiting a state machine via its Exit Node
    override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    {
        manager.PlayGame(true);
        FindObjectOfType<PauseButton>().interactable = true;
    }
}
