﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
using Zenject;

namespace HPEW.ShellSystem
{
    [RequireComponent(typeof(Rigidbody))]
    public class ShellMotionController : MonoBehaviour
    {
        [Header("Options for animation of a shot")]

        [SerializeField, Range(0f, 100f)]
        float startSpeed = 10.0f;
        public float StartSpeed => startSpeed;

        [SerializeField, Range(0f, 100f)]
        float targetSpeed = 10.0f;
        public float TargetSpeed => targetSpeed;

        [SerializeField, Range(0f, 100f)]
        float maxAccelerationAnim = 10f;
        public float MaxAccelerationAnim => maxAccelerationAnim;


        [Header("Options for gameplay move")]

        [SerializeField, Range(0f, 100f)]
        float limitSpeed = 10.0f;
        public float LimitSpeed => limitSpeed;

        [SerializeField, Range(0f, 100f)]
        float maxAcceleration = 10f;
        public float MaxAcceleration => maxAcceleration;

        [Inject] PlayerAnimationController _animator;
        [Inject] ChunkSystem.BusterSystem _busterSpawner;

        Rigidbody _rb;
        Collider _collider;

        [SerializeField] Vector3 desiredVelocity; //TODO: Serialize for test

        Action onEndGame;

        public Vector3 CurrentVelocity => _rb.velocity;

        IDisposable fixedUpdateDispose;

        public GameObject _ragdoll;
        private Transform _ragdollParent;

        private Rigidbody[] ragdollParts;
        private List<Vector3> ragdollsPartPos = new List<Vector3>();
        private List<Vector3> ragdollsPartRot = new List<Vector3>();

        private bool isBoostEnd = false;
        private Vector3 startBoostVelocity;
        private float lastSpeed = 0;

        private void Awake()
        {
            _rb = GetComponent<Rigidbody>();
            _collider = GetComponent<Collider>();

            ragdollParts = _ragdoll.GetComponentsInChildren<Rigidbody>();
            _ragdollParent = _ragdoll.transform.parent;
            foreach (var item in ragdollParts)
            {
                ragdollsPartPos.Add(item.transform.localPosition);
                ragdollsPartRot.Add(item.transform.localEulerAngles);
            }
        }

        public void StartMoving(Action action)
        {
            FindObjectOfType<PauseButton>().interactable = false;
            _animator.Dispose(false);
            
            lastSpeed = 0;
            onEndGame = action;
            _animator.Play_Run();
            //Observable.Timer(TimeSpan.FromSeconds(_animator.Animator.GetCurrentAnimatorClipInfo(0).Length + 1))
            //.Subscribe(delegate
            //{
            //    StartRunAnimation();
            //});
        }

        public void ContinueMoving(Action action)
        {
            onEndGame = action;
            StartRunAnimation();
        }

        public void StartRunAnimation()
        {
            FindObjectOfType<PauseButton>().interactable = true;
            fixedUpdateDispose?.Dispose();
            desiredVelocity = transform.forward * targetSpeed;

            fixedUpdateDispose = Observable.EveryFixedUpdate()
                .Subscribe(index =>
                {
                    if (lastSpeed == 0)
                    {
                        if (index == 0) SetVelocity(transform.forward * startSpeed, maxAccelerationAnim);
                        else SetVelocity(_rb.velocity, maxAccelerationAnim);

                        if (_rb.velocity == desiredVelocity)
                            StartGamePlayMoving();
                    }
                    else
                    {
                        SetVelocity(transform.forward * lastSpeed * 0.75f, maxAcceleration);
                        StartGamePlayMoving();
                    }

                });
        }

        public void StartIdleAnimation()
        {
            DisabledRagdoll();
            _animator.Play_Idle();
            _animator.DefaultAnimatoinSpeed();
        }

        public void ResurractionAnimation()
        {
            Observable.FromMicroCoroutine(_ => WaitShell())
                .Subscribe();
            _animator.Play_Idle();
            _animator.Play_Ressuraction();
            _animator.DefaultAnimatoinSpeed();
        }

        void StartGamePlayMoving()
        {
            fixedUpdateDispose?.Dispose();

            desiredVelocity = transform.forward * LimitSpeed;

            fixedUpdateDispose = Observable.EveryFixedUpdate()
                .Subscribe(delegate
                {
                    SetVelocity(_rb.velocity, maxAcceleration);
                    if (_rb.velocity.magnitude < targetSpeed) // End Game
                    {
                        fixedUpdateDispose?.Dispose();
                        onEndGame?.Invoke();
                        EnabledRagdoll();
                    }
                });
        }

        public void DisposeMovement()
        {
            fixedUpdateDispose?.Dispose();
            _animator.Dispose(true);
            onEndGame?.Invoke();
        }

        private void SetVelocity(Vector3 currentVelocity, float acceleration)
        {
            var xAxis = Vector3.right;
            var zAxis = Vector3.forward;

            var currentX = currentVelocity.x;
            var currentZ = currentVelocity.z;

            var maxSpeedChange = acceleration * Time.deltaTime;

            var newX = Mathf.MoveTowards(currentX, desiredVelocity.x, maxSpeedChange);
            var newZ = Mathf.MoveTowards(currentZ, desiredVelocity.z, maxSpeedChange);

            currentVelocity += xAxis * (newX - currentX) + zAxis * (newZ - currentZ);

            if (_busterSpawner.IsBoostedAutogame)
            {
                if (!isBoostEnd)
                {
                    startBoostVelocity = _rb.velocity;
                    if (startBoostVelocity.magnitude < targetSpeed)
                        startBoostVelocity = transform.forward * targetSpeed;
                    isBoostEnd = true;
                }
                _rb.velocity = new Vector3(0, 0, limitSpeed * 1.2f);
            }
            else
            {
                if (isBoostEnd)
                {
                    _rb.velocity = startBoostVelocity;
                    isBoostEnd = false;
                }
                else
                    _rb.velocity = currentVelocity;
            }
            //_rb.velocity = new Vector3(0, 0, limitSpeed);
            _animator.AnimatoinSpeed(CurrentVelocity.magnitude, limitSpeed);
            if (lastSpeed < CurrentVelocity.magnitude)
                lastSpeed = CurrentVelocity.magnitude;
        }

        IDisposable disposable;
        public void EnabledRagdoll()
        {
            DisabledRagdoll();

            _collider.enabled = false;

            _animator.transform.GetChild(1).gameObject.SetActive(false);
            _animator.transform.GetChild(0).gameObject.SetActive(false);

            _ragdoll.SetActive(true);
            _ragdoll.transform.SetParent(transform.parent);
            _ragdoll.GetComponent<Rigidbody>().AddForce(new Vector3(UnityEngine.Random.Range(-0.5f, 0.5f), UnityEngine.Random.Range(-0.5f, 0.5f), -1) * 100, ForceMode.Impulse);

            disposable = Observable.EveryLateUpdate()
                .Subscribe(_ =>
                {
                    if (_ragdoll.transform.position.y < -10)
                    {
                        DisabledRagdoll();
                    }
                });
        }

        public void DisabledRagdoll()
        {
            disposable?.Dispose();
            _ragdoll.SetActive(false);
            _ragdoll.transform.SetParent(_ragdollParent);
            for (int i = 0; i < ragdollParts.Length; i++)
            {
                ragdollParts[i].transform.localPosition = ragdollsPartPos[i];
                ragdollParts[i].transform.localEulerAngles = ragdollsPartRot[i];
            }
            _animator.transform.GetChild(1).gameObject.SetActive(true);
            _animator.transform.GetChild(0).gameObject.SetActive(true);

            _collider.enabled = true;
        }

        IEnumerator WaitShell()
        {
            while (_collider.enabled == true)
            {
                yield return null;
            }
            _collider.enabled = true;
        }
    }
}
