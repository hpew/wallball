﻿using System;
using UnityEngine;
using TMPro;
using Random = UnityEngine.Random;
using Zenject;
using UniRx;
using HPEW.Systemic;
using HPEW.ShadersAPI;

namespace HPEW.Interactive
{
    public class FallingLet : Let
    {
        [SerializeField, Range(1, 10)] int minHP = 1;
        [SerializeField, Range(1, 10)] int maxHP = 10;

        float speedDissolve;

        [SerializeField] GameObject _ParentHPStat;


        [Inject] Settings _settings;
        [Inject] Assets _assets;

        static string[] hpStringsForm0To10 = new string[]
        {
            "0","1","2","3","4","5","6","7","8","9","10"
        };

        TextMeshPro _tmpStat;
        Collider _collider;
        Rigidbody _rigidbody;

        Vector3 initLocalPos;

        FallingLevel fallingLevel;

        public Animator animator;

        protected override void Awake()
        {
            base.Awake();

            _tmpStat = GetComponentInChildren<TextMeshPro>(true);
            _collider = GetComponent<Collider>();
            _rigidbody = GetComponent<Rigidbody>();
            fallingLevel = GetComponentInChildren<FallingLevel>(true);
            speedDissolve = _settings.SpeedDissolveDestructibleLet;
        }

        public override void Restart()
        {
            _ParentHPStat.SetActive(false);
            //_dissolve.ChangeDissolveValue = 1f;
            fallingLevel.Height = 75;
            Action onFalled = null;
            onFalled = delegate
            {
                _ParentHPStat.SetActive(true);
                //_dissolve.OnAppear -= onAppear;
                fallingLevel.OnFall -= onFalled;
            };
            //_dissolve.OnAppear += onAppear;
            //_dissolve.StartAnimationAppear(speedDissolve);
            //fallingLevel.Acceleration = Random.Range(0.1f, 0.981f);
            fallingLevel.OnFall += onFalled;
            fallingLevel.FallingAnimation();


            HP = Random.Range(minHP, maxHP + 1);

            if (_tmpStat == null)
             _tmpStat = GetComponentInChildren<TextMeshPro>(true);
            _tmpStat.SetText(hpStringsForm0To10[HP]);
            _tmpStat.gameObject.SetActive(true);

            if (_collider == null)
                _collider = GetComponent<Collider>();

            _collider.enabled = true;

            isInteractive = true;
        }

        public override void Interact(object data = null)
        {

            HP--;
            _tmpStat.SetText(hpStringsForm0To10[Mathf.Clamp(HP, 0, 10)]);

            if (HP > 0) WobbleLet();
            else
            {
                isInteractive = false;
                InvokeOnDestroyed();
                Fall();
            }
        }

        public override void AutoInteract(object data = null)
        {
            //if (!(data is InvincibilityBuster)) return;
            isInteractive = false;
            InvokeOnDestroyed();
            Fall();
        }

        private void WobbleLet()
        {
            // Шатание 
            animator.SetTrigger("Wiggle");
        }

        private void Fall()
        {
            // Падение
            animator.SetTrigger("Open");
            _tmpStat.gameObject.SetActive(false);
            //_rigidbody.useGravity = true;
            //_rigidbody.isKinematic = false;
            Observable.Timer(TimeSpan.FromSeconds(0.1))
                .Subscribe(_ => 
                {
                    _collider.enabled = false;
                });
        }

        public override void Sleep()
        {
            // Исходное положение
            //_rigidbody.useGravity = false;
            //_rigidbody.isKinematic = true;
            //transform.eulerAngles = Vector3.zero;
            _collider.enabled = true;
            animator.SetTrigger("Reset");
        }
    }
}