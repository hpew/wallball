﻿using System;
using UnityEngine;
using TMPro;
using Random = UnityEngine.Random;
using Zenject;
using UniRx;
using HPEW.Systemic;
using HPEW.ShadersAPI;

namespace HPEW.Interactive
{
    public class DestructibleLet : Let
    {
        [SerializeField, Range(1, 10)] int minHP = 1;
        [SerializeField, Range(1, 10)] int maxHP = 10;

        [SerializeField] float forceSphereExplode = 1.0f;
        [SerializeField] float forceShock = 100f;

        float speedDissolve;

        [SerializeField] Rigidbody _shockBody; //TODO заменить на класс-контракт

        [SerializeField] GameObject _physicModel;
        [SerializeField] GameObject _uniformModel; //for shader;

        [SerializeField] GameObject _ParentHPStat;


        [Inject] Settings _settings;
        [Inject] Assets _assets;

        static string[] hpStringsForm0To10 = new string[]
        {
            "0","1","2","3","4","5","6","7","8","9","10"
        };

        TextMeshPro _tmpStat;
        PartDestructibleLet[] _spheres;
        Collider _collider;

        Vector3[] initLocalPosSpheres;
        Vector3 initLocalPosShockBody;

        //DissolveAPI _dissolve;
        FallingLevel fallingLevel;

        protected override void Awake()
        {
            base.Awake();

            _tmpStat = GetComponentInChildren<TextMeshPro>(true);
            _collider = GetComponent<Collider>();
            _spheres = GetComponentsInChildren<PartDestructibleLet>(true);

            initLocalPosSpheres = new Vector3[_spheres.Length];
            for (int i = 0; i < _spheres.Length; i++)
            {
                initLocalPosSpheres[i] = _spheres[i].transform.localPosition;
            }

            initLocalPosShockBody = _shockBody.transform.localPosition;

            //_dissolve = GetComponentInChildren<DissolveAPI>(true);
            fallingLevel = GetComponentInChildren<FallingLevel>(true);
            speedDissolve = _settings.SpeedDissolveDestructibleLet;
        }

        public override void Restart()
        {
            _physicModel.SetActive(false);
            _uniformModel.SetActive(true);
            _ParentHPStat.SetActive(false);
            //_dissolve.ChangeDissolveValue = 1f;
            fallingLevel.Height = 75;
            Action onFalled = null;
            onFalled = delegate
            {
                _physicModel.SetActive(true);
                _uniformModel.SetActive(false);
                _ParentHPStat.SetActive(true);
                //_dissolve.OnAppear -= onAppear;
                fallingLevel.OnFall -= onFalled;
            };
            //_dissolve.OnAppear += onAppear;
            //_dissolve.StartAnimationAppear(speedDissolve);
            //fallingLevel.Acceleration = Random.Range(0.1f, 0.981f);
            fallingLevel.OnFall += onFalled;
            fallingLevel.FallingAnimation();


            HP = Random.Range(minHP, maxHP + 1);

            if (_tmpStat == null)
             _tmpStat = GetComponentInChildren<TextMeshPro>(true);
            _tmpStat.SetText(hpStringsForm0To10[HP]);
            _tmpStat.gameObject.SetActive(true);

            if (_collider == null)
                _collider = GetComponent<Collider>();

            _collider.enabled = true;

            if (_spheres == null)
                _spheres = GetComponentsInChildren<PartDestructibleLet>(true);

            for (int i = 0; i < initLocalPosSpheres.Length; i++)
            {
                _spheres[i].DisablePhysics();
                _spheres[i].transform.localPosition = initLocalPosSpheres[i];
            }

            isInteractive = true;
        }

        public override void Interact(object data = null)
        {

            HP--;
            _tmpStat.SetText(hpStringsForm0To10[Mathf.Clamp(HP, 0, 10)]);

            if (HP > 0) BreakPart();
            else
            {
                isInteractive = false;
                InvokeOnDestroyed();
                Explode();
            }
        }

        public override void AutoInteract(object data = null)
        {
            //if (!(data is InvincibilityBuster)) return;
            isInteractive = false;
            InvokeOnDestroyed();
            Explode();
        }

        private void BreakPart()
        {
            var count = 0;
            int index;
            do index = Random.Range(0, _spheres.Length);
            while (_spheres[index].IsKinematic == false && count++ < _spheres.Length);

            PushPart(_spheres[index]);
        }

        private void Explode()
        {
            _collider.enabled = false;
            _tmpStat.gameObject.SetActive(false);
            _shockBody.isKinematic = false;

            foreach (var sphere in _spheres)
            {
                sphere.EnabledPhysics();
            }

            var dispose = Observable.EveryLateUpdate()
                .Subscribe(delegate 
                {
                    _shockBody.velocity = transform.forward * forceShock;
                });

            Observable.Timer(TimeSpan.FromSeconds(0.5f))
                .Subscribe(delegate
                {
                    _shockBody.velocity = Vector3.zero;
                    dispose.Dispose();
                });
        }

        private void PushPart(PartDestructibleLet sphere)
        {
            var center = _collider.bounds.center;

            var z = Random.Range(-3.0f, -1.0f);
            var explosionPos = new Vector3(center.x, center.y, center.z + z);

            sphere.PushOff(forceSphereExplode, explosionPos, 0);
        }

        public override void Sleep()
        {
            for (int i = 0; i < _spheres.Length; i++)
            {
                var sphere = _spheres[i];
                sphere.DisablePhysics();
                sphere.transform.localPosition = initLocalPosSpheres[i];
            }

            _shockBody.transform.localPosition = initLocalPosShockBody;
        }
    }
}