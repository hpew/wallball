﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace HPEW.Interactive
{

    public class PartDestructibleLet : MonoBehaviour
    {
        public bool IsKinematic => _body.isKinematic;

        Rigidbody _body;
        Collider _collider;

        Quaternion initRotation;

        private void Awake()
        {
            _body = GetComponent<Rigidbody>();
            _collider = GetComponent<Collider>();

            initRotation = transform.localRotation;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.gameObject.layer == 12) gameObject.layer = 11;
        }

        public void DisablePhysics()
        {
            _body.isKinematic = true;
            _collider.enabled = false;
            _body.useGravity = false;
            gameObject.layer = 10;

            transform.localRotation = initRotation;
        }

        public void EnabledPhysics()
        {
            _body.isKinematic = false;
            _collider.enabled = true;
            _body.useGravity = true;
        }

        public void PushOff(float force, Vector3 explosionPos, float radius)
        {
            _body.useGravity = false;
            _body.isKinematic = false;
            _collider.enabled = true;
            _body.AddExplosionForce(force, explosionPos, radius);
        }
    }
}