﻿using UnityEngine;
using TMPro;
using StopWatch = System.Diagnostics.Stopwatch;
using Random = UnityEngine.Random;
using DestroyIt;
using Zenject;
using HPEW.Systemic;


namespace HPEW.Interactive
{
    [RequireComponent(typeof(Destructible))]
    public class TouchLet : Let
    {
        [SerializeField, Range(1, 10)] int minHP = 1;
        [SerializeField, Range(1, 10)] int maxHP = 10;

        TextMeshPro _tmpStat;
        Destructible _destructible;

        [Inject] Settings _settings;
        [Inject] Assets _assets;

        static string[] hpStringsForm0To10 = new string[]
        {
            "0","1","2","3","4","5","6","7","8","9","10"
        };

        protected override void Awake()
        {
            base.Awake();
            HP = Random.Range(minHP, maxHP + 1);

            _destructible = GetComponent<Destructible>();
            _tmpStat = GetComponentInChildren<TextMeshPro>(true);
            _tmpStat.SetText(hpStringsForm0To10[Mathf.Clamp(HP, 0, 10)]);
            _tmpStat.gameObject.SetActive(true);

            _tmpStat.gameObject.SetActive(false);

            _destructible.totalHitPoints = _destructible.currentHitPoints = HP;
            _destructible.damageLevels = _settings.GetDamageLevels(HP);

        }

        public override void Interact(object data = null)
        {
          //  if (HP > 0) HPPopup.Create(_assets.HpPopup.transform, transform.position + Vector3.back, hpStringsForm0To10[Mathf.Clamp(HP, 0, 10)]);
            HP--;
            _tmpStat.SetText(hpStringsForm0To10[Mathf.Clamp(HP, 0, 10)]);
            if (HP <= 0) isInteractive = false;

            _destructible.ApplyDamage(1f);
        }

        public override void AutoInteract(object data = null)
        {
        }

        public override void Restart()
        {
            throw new System.NotImplementedException();
        }

        public override void Sleep()
        {

        }
    }
}