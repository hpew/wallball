﻿using UnityEngine;
using HPEW.ShellSystem;
using Zenject;
using UniRx;
using System;
using System.Collections;

namespace HPEW.ChunkSystem
{
    public class CameraBehaviour : MonoBehaviour
    {
        [SerializeField, Range(0f, 1f)]
        float smoothTime = 0.5f;

        [SerializeField, Range(0f, 100f)]
        float activationDistance = 10f;

        [SerializeField, Range(0f, 10f)]
        float mainMenuDistance = 2f;

        Vector3 offset;
        Vector3 velocity = Vector3.zero;

        Quaternion _startRotation;
        Quaternion? _targetRotation;
        Vector3 _startCameraRotation;
        float _degVelocity;
        bool isShaking;

        [Inject] Shell _shell;
        Transform _focus;

        IDisposable disposable;
        public bool IsMainMenu { get; private set; } = true;

        private void Awake()
        {
            _focus = _shell.transform;
            offset = transform.position - _focus.position;
        }

        private void Start()
        {
            _startRotation = transform.localRotation;
            _startCameraRotation = Camera.main.transform.eulerAngles;
        }

        private void LateUpdate()
        {
            if (isShaking) return;
            var desiredPosition = _focus.position + offset;
            var desiredRotation = _startCameraRotation;
            if (IsMainMenu)
            {
                desiredPosition -= Vector3.up * mainMenuDistance;
            }
            var smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothTime);
            transform.position = smoothedPosition;

            //Camera.main.transform.eulerAngles = Vector3.SmoothDamp(Camera.main.transform.eulerAngles, desiredRotation, ref velocity, smoothTime);

        }

        private void Update()
        {
            Quaternion target = _targetRotation == null ? _startRotation : _targetRotation.Value;
            if (target == transform.localRotation)
            {
                return;
            }

            float t = (Time.deltaTime * _degVelocity) / Quaternion.Angle(transform.localRotation, target);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, target, t);
            if (transform.localRotation == _targetRotation)
            {
                _targetRotation = null;
            }
        }
        public void SetIsMainMenu(bool f)
        {
            IsMainMenu = f;
            //Camera.main.transform.rotation = _startCameraRotation;
        }

        public void ShakeCamera(float duration, float maxAngle, float degVelocity)
        {
            disposable?.Dispose();
            isShaking = false;
            disposable = Observable.FromCoroutine(_ => VibrateCameraCor(duration, maxAngle, degVelocity))
                .Subscribe();

        }

        public void ShakeRotateCamera(Vector2 direction, float angleDeg, float degVelocity)
        {
            if (isShaking)
            {
                return;
            }
            ShakeRotateCameraInternal(direction, angleDeg, degVelocity);
        }

        private void ShakeRotateCameraInternal(Vector2 direction, float angleDeg, float degVelocity)
        {
            //direction *= -1;

            _degVelocity = degVelocity;
            direction = direction.normalized;
            direction *= Mathf.Tan(angleDeg * Mathf.Deg2Rad);
            Vector3 resDirection = ((Vector3)direction + transform.forward).normalized;
            _targetRotation = Quaternion.FromToRotation(transform.forward, resDirection);
        }

        IEnumerator VibrateCameraCor(float duration, float maxAngle, float degVelocity)
        {
            isShaking = true;
            float elapsed = 0f;
            float timePassed = Time.realtimeSinceStartup;
            while (elapsed < duration)
            {
                float currentTime = Time.realtimeSinceStartup;
                elapsed += currentTime - timePassed;
                timePassed = currentTime;

                ShakeRotateCameraInternal(UnityEngine.Random.insideUnitCircle, UnityEngine.Random.Range(0, maxAngle), degVelocity);

                yield return new WaitForSeconds(0.05f);
            }

            isShaking = false;
        }
    }
}
