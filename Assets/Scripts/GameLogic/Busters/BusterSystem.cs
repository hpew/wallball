﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using HPEW.Pool;
using HPEW.ShellSystem;
using HPEW.Input;
using HPEW.Systemic;
using HPEW.Interactive;

namespace HPEW.ChunkSystem
{
    public class BusterSystem
    {
        PoolBuster[] _poolBusters;
        InputSystem _inputSystem;
        Settings _settings;
        Assets _assets;
        GameResourses _resourses;
        PlayerAnimationController _animator;
        public event Action<object, Action> OnBusterInteract;

        float BusterDuration = 0;

        public bool IsBoostedAutogame { get; private set; } = false;
        public bool IsBoostedMagnete { get; private set; } = false;
        public int BustedBag { get; set; } = 0;
        public BusterSystem(PoolBuster[] poolBusters, InputSystem inputSystem,
            Settings settings, Assets assets, GameResourses resourses, PlayerAnimationController animator)
        {
            _poolBusters = poolBusters;
            _inputSystem = inputSystem;
            _settings = settings;
            _assets = assets;
            _resourses = resourses;
            _animator = animator;

            foreach (var buster in _assets.BusterPrefabs)
            {
                if (buster is AutogameBuster)
                {
                    resourses.UI.autogameButton.onClick.AddListener(() =>
                    {
                        if (_resourses.AutogameCount > 0 && !IsBoostedAutogame)
                        {
                            _resourses.ChangeAutogameCount(-1);
                            UseBuster(buster);
                        }
                    });
                }

                if (buster is CoinsbagBuster)
                {
                    resourses.UI.coinsbagButton.onClick.AddListener(() =>
                    {
                        if (_resourses.CoinsBagCount > 0 && BustedBag == 0)
                        {
                            _resourses.ChangeCoinsBagCount(-1);
                            UseBuster(buster);
                        }
                    });
                }

                if (buster is MagnetBuster)
                {
                    resourses.UI.magnetButton.onClick.AddListener(() =>
                    {
                        if (_resourses.MagneteCount > 0 && !IsBoostedMagnete)
                        {
                            _resourses.ChangeMagneteCount(-1);
                            UseBuster(buster);
                        }
                    });
                }
            }
        }



        public Buster Spawn(bool isCoinsOnly = false)
        {
            Math.WeightedRandom<int> random = new Math.WeightedRandom<int>();
            random.AddEntry(0, _settings.CoinSpawnСhance);
            if (_assets.BusterPrefabs.Length > 0 && !isCoinsOnly)
                for (int i = 1; i < _assets.BusterPrefabs.Length; i++)
                {
                    random.AddEntry(i, _settings.BusterSpawnСhance);
                }

            var indexPool = random.GetRandom();

            var buster = _poolBusters[indexPool].Spawn();
            //buster.transform.SetParent(_shell.transform);
            //buster.transform.localPosition = new Vector3(-2, 9, 0);

            InputSystem.RaycastTouch inputHandler = null;
            inputHandler = (type) =>
            {
                if (buster != null && type == buster.gameObject)
                {
                    buster.TouchHandler();
                    //for (int i = 0; i < buster.Duration; i++)
                    //{
                    //    OnBusterInteract?.Invoke(buster);
                    //}
                    //Debug.Log(buster.name);
                }
            };

            Action handler = null;
            handler = delegate
            {
                buster.OnDestroyed -= handler;
                _inputSystem.OnRaycastTouch -= inputHandler;
                _poolBusters[indexPool].Despawn(buster);
            };
            buster.OnDestroyed += handler;
            _inputSystem.OnRaycastTouch += inputHandler;
            return buster;
        }

        private void UseBuster(Buster buster)
        {
            Action handler = null;
            if (buster is AutogameBuster)
            {
                IsBoostedAutogame = true;
                _animator.Play_Sprint(true);
                handler = () =>
                {
                    IsBoostedAutogame = false;
                    _animator.Play_Sprint(false);
                };
            }

            if (buster is CoinsbagBuster)
            {
                handler = () =>
                {
                    BustedBag = 0;
                };
            }

            if (buster is MagnetBuster)
            {
                IsBoostedMagnete = true;
                handler = () => IsBoostedMagnete = false;
            }

            for (int i = 0; i < buster.Duration; i++)
            {
                OnBusterInteract?.Invoke(buster, handler);
            }

        }
    }
}
