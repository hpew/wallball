﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Zenject;
using HPEW.ChunkSystem;
using HPEW.ShellSystem;

namespace HPEW.Interactive
{
    public class MagneteZone : MonoBehaviour
    {
        [SerializeField, Range(1, 100)] int speed = 10;
        
        public GameObject target;

        [Inject] BusterSystem _busterSystem;
        [Inject] Shell player;

        Collider _collider;

        private void Start()
        {
            _busterSystem.OnBusterInteract += ActivateMagnete;
            _collider = GetComponent<Collider>();
            _collider.enabled = false;
            target.transform.parent.gameObject.SetActive(false);            
        }

        private void OnTriggerEnter(Collider other)
        {
            var coins = other.GetComponent<Coins>();
            Debug.Log(other.gameObject.name);

            if (coins == null) return;

            Observable.FromMicroCoroutine(_ => MoveCoinsToMagnete(coins))
                 .Subscribe();
        }

        IEnumerator MoveCoinsToMagnete(Coins coins)
        {
            while (target.transform.position.z - coins.transform.position.z < 0.01f)
            {
                coins.transform.position = Vector3.MoveTowards(coins.transform.position, target.transform.position, Time.deltaTime * speed);
                yield return null;
            }
            coins.Interact();
        }

        private void ActivateMagnete(object data, System.Action action)
        {
            if (!(data is MagnetBuster)) return;
            _collider.enabled = true;
            target.transform.parent.gameObject.SetActive(true);
            System.Action handler = null;
            handler = delegate
            {
                _collider.enabled = false;
                target.transform.parent.gameObject.SetActive(false);
                action?.Invoke();
                player.OnEndGame -= handler;
            };

            player.OnEndGame += handler;
            Observable.Timer(System.TimeSpan.FromSeconds((data as MagnetBuster).Duration))
                .Subscribe(_=>
                {
                    handler?.Invoke();
                });
        }
    }
}
