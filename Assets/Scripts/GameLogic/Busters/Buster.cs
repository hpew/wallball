﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Zenject;
using HPEW.Input;
using UniRx;

namespace HPEW.Interactive
{
    public abstract class Buster : MonoBehaviour
    {
        [SerializeField, Range(1, 60)] int duration = 1;
        public int Duration => duration;
        public bool isInteractive { get; protected set; } = true;

        public event Action OnDestroyed = delegate { };

        [Inject] protected GameResourses resourses;

        public ParticleSystem particle;

        protected virtual void Awake()
        {
           
        }

        public virtual void TouchHandler()
        {
            PlayParticle();
            Interact();
        }

        public abstract void Interact();

        public abstract void Restart();

        public abstract void Sleep();

        protected void InvokeOnDestroyed()
        {
            OnDestroyed?.Invoke();
        }

        public void DestroyBuster()
        {
            InvokeOnDestroyed();
        }

        protected void PlayParticle()
        {
            if (particle == null) return;
            particle.transform.parent = transform.parent;
            particle.transform.localScale = Vector3.one;
            particle.transform.position = transform.position;
            particle.Play();
            Observable.Timer(TimeSpan.FromSeconds(particle.main.duration))
                .Subscribe(delegate
                {
                    if (particle == null) return;
                    particle.Stop();
                    particle.transform.parent = transform;
                });
        }
    }
}
