﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPEW.Interactive
{
    public class MagnetBuster : Buster
    {

        protected override void Awake()
        {
            base.Awake();
        }

        public override void Restart()
        {

        }

        public override void Sleep()
        {

        }

        public override void Interact()
        {
            Debug.Log("Interact Magnet");
            resourses.ChangeMagneteCount(1);
            InvokeOnDestroyed();
        }
    }
}
