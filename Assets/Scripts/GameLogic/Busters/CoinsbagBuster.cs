﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPEW.Interactive
{
    public class CoinsbagBuster : Buster
    {
        [SerializeField, Range(1, 100)] int density = 1;
        public int Density => density;
        protected override void Awake()
        {
            base.Awake();
        }

        public override void Restart()
        {

        }

        public override void Sleep()
        {

        }

        public override void Interact()
        {
            Debug.Log("Interact coinsbag");
            resourses.ChangeCoinsBagCount(1);
            InvokeOnDestroyed();
        }
    }
}
