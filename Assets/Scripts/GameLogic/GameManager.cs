﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using HPEW.ShellSystem;
using HPEW.ChunkSystem;
using HPEW.UI;
using UniRx;

public class GameManager : MonoBehaviour
{
    [Inject] Shell player;
    [Inject] ShellMotionController playerMotion;
    [Inject] ChunksPlacer placer;
    [Inject] CameraBehaviour cameraBehaviour;
    [Inject] Score score;
    [Inject(Id = "Castle")] Animator castleAnimator;
    [Inject] UIManager UIManager;
    public event Action OnRestart;
    public event Action OnResurract;

    private Vector3 startPlayerPosition;

    public bool IsDead { get; set; } = false;

    IDisposable disposable;
    private void Awake()
    {
        startPlayerPosition = player.gameObject.transform.position;
        Application.targetFrameRate = 30;
    }

    private void Start()
    {
        MessageBroker.Default
       .Receive<Message>()
       .Subscribe(msg =>
       {
           if (msg.button is GameStartButton)
           {
               PlayGame();
               UIManager.Get<GameplayWindow>().Open();
           }
           else if (msg.button is RestartButton)
           {
               RestartGame();
               UIManager.Get<GameplayWindow>().Close();
               UIManager.Get<MainMenuWindow>().Open();
           }
           else if (msg.button is ContinueGameButton)
           {
               Resurract();
               UIManager.Get<GameplayWindow>().GameoverWindowClose();
           }
           else if (msg.button is SkinShopButton)
           {
               UIManager.Get<SkinsWindow>().Open();
           }
           else if (msg.button is ExitButton)
           {
               Window.CurrentWindow.Close();
           }
           else if (msg.button is PauseButton)
           {
               UIManager.Get<GameplayWindow>().PauseWindowOpen();
           }
       });

        MessageBroker.Default
      .Receive<ConfirmMessage>()
      .Subscribe(msg =>
      {
          if (msg.windowParent is PauseWindow)
          {
              if (msg.confirmType == ConfirmType.yes)
              {
                  player.OnEndGame -= EndGame;
                  playerMotion.DisposeMovement();
                  BreakGame();
                  RestartGame();
                  UIManager.Get<GameplayWindow>().Close();
                  UIManager.Get<MainMenuWindow>().Open();
              }
              else if (msg.confirmType == ConfirmType.no)
              {
                  UIManager.Get<GameplayWindow>().PauseWindowClose();
              }
          }
      });
    }

    public void PlayGame(bool isContinue = false)
    {
        cameraBehaviour.SetIsMainMenu(false);
        castleAnimator.SetBool("Open", true);
        player.GetComponent<Rigidbody>().isKinematic = false;
        player.OnEndGame += EndGame;
        Debug.Log("<color=green>STARTGAME</color>");
        if (isContinue)
            player.StartContinue();
        else
            player.StartShell();
        score.SetScore(0);
        disposable = Observable.EveryUpdate()
            .Subscribe(_ =>
            {
                score.SetScore((int)(player.gameObject.transform.position.z / 10));
            });
    }

    public void EndGame()
    {
        //castleAnimator.SetBool("Open", false);
        cameraBehaviour.ShakeRotateCamera(UnityEngine.Random.insideUnitCircle, 5, 100);
        //placer.DestroyBusters();
        //IsDead = true;

        player.OnEndGame -= EndGame;
        Debug.Log("<color=red>ENDGAME</color>");
        UIManager.Get<GameplayWindow>().GameoverWindowOpen();
        //disposable?.Dispose();
        //score.SetLastScore(score.CurrentScore);
        BreakGame();
    }

    private void BreakGame()
    {
        castleAnimator.SetBool("Open", false);
        placer.DestroyBusters();
        IsDead = true;
        Debug.Log("<color=yellow>BREAKGAME</color>");
        disposable?.Dispose();
        score.SetLastScore(score.CurrentScore);
    }

    public void RestartGame()
    {
        IsDead = false;
        cameraBehaviour.transform.position = new Vector3(0, 15, -20);//startPlayerPosition;
        cameraBehaviour.SetIsMainMenu(true);
        player.GetComponent<Rigidbody>().isKinematic = true;
        playerMotion.StartIdleAnimation();
        OnRestart?.Invoke();
        player.gameObject.transform.position = startPlayerPosition;

        placer.ResetChunk();
        score.SetScore(0);
    }

    public void Resurract()
    {
        IsDead = false;
        cameraBehaviour.enabled = true;
        player.GetComponent<Rigidbody>().isKinematic = true;
        playerMotion.ResurractionAnimation();

        OnResurract?.Invoke();
    }

    void Update()
    {
        FPS();
    }

    float deltaTime;
    public Text fpsText;
    void FPS()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        float fps = 1.0f / deltaTime;
        fpsText.text = Mathf.Ceil(fps).ToString();
    }

    public class Message
    {
        public Button button { get; private set; }
        public Message(Button button)
        {
            this.button = button;
        }

        public static Message Create(Button button)
        {
            return new Message(button);
        }
    }

    public class ConfirmMessage
    {
        public Window windowParent { get; private set; }
        public ConfirmType confirmType { get; private set; }
        public ConfirmMessage(ConfirmType confirmType, Window windowParent)
        {
            this.confirmType = confirmType;
            this.windowParent = windowParent;
        }

        public static ConfirmMessage Create(ConfirmType confirmType, Window windowParent)
        {
            return new ConfirmMessage(confirmType, windowParent);
        }
    }
}
