﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HPEW.ShellSystem;
using UniRx;

public class CheckpointTrigger : MonoBehaviour
{
    ParticleSystem particle;

    private void Awake()
    {
        particle = gameObject.transform.parent.GetChild(1).gameObject.GetComponent<ParticleSystem>();
        
    }

    private void OnTriggerEnter(Collider other)
    {
        var obj = other.gameObject.GetComponent<Shell>();
        if (obj == null) return;

        Debug.Log("Triggered");
        gameObject.SetActive(false);
        gameObject.transform.parent.GetChild(1).gameObject.SetActive(true);

        Observable.Timer(System.TimeSpan.FromSeconds(particle.main.duration))
            .Subscribe(delegate
            {
                gameObject.SetActive(true);
                gameObject.transform.parent.GetChild(1).gameObject.SetActive(false);
            });
    }
}
