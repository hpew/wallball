﻿using UnityEngine;
using UnityEngine.UI;

namespace HPEW.DebugTools
{
	
	public class FPSDisplay : MonoBehaviour
	{
		static string[] stringsFrom00To99 =
		{
		"00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
		"10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
		"20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
		"30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
		"40", "41", "42", "43", "44", "45", "46", "47", "48", "49",
		"50", "51", "52", "53", "54", "55", "56", "57", "58", "59",
		"60", "61", "62", "63", "64", "65", "66", "67", "68", "69",
		"70", "71", "72", "73", "74", "75", "76", "77", "78", "79",
		"80", "81", "82", "83", "84", "85", "86", "87", "88", "89",
		"90", "91", "92", "93", "94", "95", "96", "97", "98", "99"
		};

		[System.Serializable]
		private struct FPSColor
		{
			public Color color;
			public int minimumFPS;
		}

		[SerializeField] FPSColor[] coloring;

		FPSCounter fpsCounter;

		int width;
		int height;
		GUIStyle labelStyle;

		public bool Show = true;


		Rect[] rects;

		void Awake()
		{
			width = Screen.width;
			height = Screen.height;

			rects = new Rect[]
			{
				new Rect(10,10,width - 20, height - 20), //averrage
				new Rect(10,60,width - 20, height - 20), //high
				new Rect(10,110,width - 20, height - 20), //low
			};

			fpsCounter = new FPSCounter();
		}

		void OnGUI()
		{
			if (!Show) return;

			// Display the label at the center of the window.
		    labelStyle = new GUIStyle(GUI.skin.GetStyle("label"));
		    labelStyle.alignment = TextAnchor.UpperLeft;

			var size = 18;

#if UNITY_ANDROID && !UNITY_EDITOR
            size = 8;
#endif

			// Modify the size of the font based on the window.
			labelStyle.fontSize = size * (width / 200);

			Display(rects[0], fpsCounter.AverageFPS);
			Display(rects[1], fpsCounter.HighestFPS);
			Display(rects[2], fpsCounter.LowestFPS);
		}

		void Display(Rect rect, int fps)
		{
			var @default = GUI.contentColor;
			for (int i = 0; i < coloring.Length; i++)
			{
				if (fps >= coloring[i].minimumFPS)
				{
					GUI.contentColor = coloring[i].color;
					break;
				}
			}

			GUI.Label(rect, stringsFrom00To99[Mathf.Clamp(fps, 0, 99)], labelStyle);

			GUI.contentColor = @default;

		}
	}
}