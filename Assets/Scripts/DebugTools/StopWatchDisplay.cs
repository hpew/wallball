﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPEW.DebugTools
{
    public class StopWatchDisplay : MonoBehaviour
    {
        int width;
        int height;
        Rect rect;
        GUIStyle labelStyle;
        string currentTime;

        void Awake()
        { 
            width = Screen.width;
            height = Screen.height;
            rect = new Rect(10, 10, width - 20, height - 20);
        }

        void OnGUI()
        {
            // Display the label at the center of the window.
            labelStyle = new GUIStyle(GUI.skin.GetStyle("label"));
            labelStyle.alignment = TextAnchor.LowerLeft;

            // Modify the size of the font based on the window.
            labelStyle.fontSize = 12 * (width / 200);

            // Obtain the current time.
            currentTime = Time.time.ToString("f3");
            currentTime = "Время: " + currentTime + " с.";

            // Display the current time.
            GUI.Label(rect, currentTime, labelStyle);
        }
    }
}