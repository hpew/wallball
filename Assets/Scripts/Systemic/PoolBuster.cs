﻿using Zenject;
using HPEW.Interactive;

namespace HPEW.Pool
{
    public class PoolBuster : MonoMemoryPool<Buster>
    {
        protected override void Reinitialize(Buster buster)
        {
            buster.Restart();
        }

        protected override void OnDespawned(Buster buster)
        {
            base.OnDespawned(buster);
            buster.Sleep();
        }
    }
}
