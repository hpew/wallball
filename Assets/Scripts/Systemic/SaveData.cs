﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveData
{
    Dictionary<string, object> saveData;

    public bool IsLoadData { get; private set; }

    public SaveData()
    {
        IsLoadData = false;
    }

    public void Save()
    {
        if (IsLoadData == false) throw new Exception("Dont load SaveData!");
        SetDataToPlayerPrefs(SaveDataToBytes(saveData));
    }

    public void Load()
    {
        if (IsLoadData == true) return;
        saveData = BytesToSaveData(GetDataFromPlayerPrefs());
        IsLoadData = true;
    }

    public void SetSave(string key, object data)
    {
        if (IsLoadData == false) throw new Exception("Dont load SaveData!");

        if (!saveData.ContainsKey(key))
            saveData.Add(key, data);
        else
            saveData[key] = data;
    }

    public object GetSave(string key, object defaultData = null)
    {
        if (IsLoadData == false) throw new Exception("Dont load SaveData!");

        if (!saveData.ContainsKey(key))
        {
            return defaultData;
        }

        return saveData[key];
    }

    private byte[] GetDataFromPlayerPrefs()
    {
        return Convert.FromBase64String(PlayerPrefs.GetString("SaveData"));
    }

    private void SetDataToPlayerPrefs(byte[] data)
    {
        PlayerPrefs.SetString("SaveData", Convert.ToBase64String(data));
    }

    private byte[] SaveDataToBytes(Dictionary<string, object> data)
    {
        byte[] result;
        using (var memoryStream = new MemoryStream())
        {
            BinaryFormatter bf = new BinaryFormatter();

            bf.Serialize(memoryStream, data);
            result = memoryStream.ToArray();
        }

        return result;
    }

    private Dictionary<string, object> BytesToSaveData(byte[] data)
    {
        Dictionary<string, object> result = new Dictionary<string, object>();

        if (data == null) return result;

        using (var memoryStream = new MemoryStream())
        {
            BinaryFormatter bf = new BinaryFormatter();

            memoryStream.Write(data, 0, data.Length);
            memoryStream.Seek(0, SeekOrigin.Begin);
            try
            {
                result = (Dictionary<string, object>)bf.Deserialize(memoryStream);
            }
            catch
            {
                return result;
            }
        }

        return result;
    }
}
