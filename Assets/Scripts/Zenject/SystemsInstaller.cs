﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using HPEW.ChunkSystem;
using HPEW.Pool;
using HPEW.Interactive;

namespace HPEW.Zenject
{
    public class SystemsInstaller : MonoInstaller
    {
        [Inject] Systemic.Assets _assets;
        [Inject] Systemic.Settings _settings;

        public override void InstallBindings()
        {
            Container.Bind<ChunksPlacer>()
                .AsSingle()
                .NonLazy();

            Container.Bind<BusterSystem>()
                .AsSingle()
                .NonLazy();

            Container.Bind<GameResourses>()
                .AsSingle()
                .NonLazy();

            Container.Bind<SaveData>()
                .AsSingle()
                .NonLazy();

            Container.Bind<Score>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<UIManager>()
               .AsSingle()
               .NonLazy();

            var lets = _assets.LetPrefabs;

            foreach (var prefab in lets)
            {
                Container.BindMemoryPool<Let, PoolLet>()
                    .WithInitialSize(_settings.InitialSize)
                    .FromComponentInNewPrefab(prefab)
                    .UnderTransformGroup("PoolContainer - " + prefab.name);
            }

            var busters = _assets.BusterPrefabs;

            foreach (var prefab in busters)
            {
                Container.BindMemoryPool<Buster, PoolBuster>()
                    .WithInitialSize(_settings.InitialSize)
                    .FromComponentInNewPrefab(prefab)
                    .UnderTransformGroup("PoolContainer - " + prefab.name);
            }
        }
    }
}
