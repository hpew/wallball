Shader "HPEW/Rim"
{
    Properties{
        _MainTex("Base (RGB)", 2D) = "white" {}
        [HDR] _Color("Color", Color) = (1, 1, 1, 1)
        _RimValue("Rim value", Range(0, 1)) = 0.5
        _Smoothness("Smooth texture",Range(0, 1)) = 0
    }
        SubShader{
            Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }

            CGPROGRAM
            #pragma surface surf Unlit vertex:vert alpha

            #pragma target 2.0

            sampler2D _MainTex;
            fixed _RimValue;
            float _Smoothness;

            struct Input
            {
                float2 uv_MainTex;
                float rim;
            };

            half4 LightingUnlit(SurfaceOutput s, half3 lightDir, half atten) {
                half4 c;
                c.rgb = s.Albedo;
                c.a = s.Alpha;
                return c;
            }

            void vert(inout appdata_full v, out Input o) 
            {
                UNITY_INITIALIZE_OUTPUT(Input, o);

                float3 normal = normalize(UnityObjectToWorldNormal(v.normal));
                float3 dir = normalize(WorldSpaceViewDir(v.vertex));
                float val = 1 - (abs(dot(dir, normal)));
                o.rim = val * val * _RimValue;
            }

            fixed4 _Color;

            void surf(Input IN, inout SurfaceOutput o)
            {
                half4 c = tex2D(_MainTex, IN.uv_MainTex + _Time.y * 0.1);

                c.r = max(_Smoothness, c.r);
                c.g = max(_Smoothness, c.g);
                c.b = max(_Smoothness, c.b);

                o.Albedo = c.rgb * _Color;

                o.Alpha = c.a * IN.rim;
            }
            ENDCG
        }
            FallBack "Diffuse"
}
