Shader "HPEW/GhostShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_GhostColor("Ghost Color", Color) = (1,1,1,1)
		_Offset("Offset", Range(0, 2)) = 0 
		_GhostAlpha("Ghost Alpha", Range(0, 1)) = 1 
		_ShakeSpeed("Shake Speed", Range(0, 50)) = 1 
		_ShakeDir("Shake Direction", Vector) = (0, 0, 1, 0) 
		_Control("Control", Range(0, 1)) = 0 

		[Header(Frensel)]
		[Space(10)]

		_FresnelColor("Fresnel Color", Color) = (1,1,1,1)
		_FresnelBias("Fresnel Bias", Float) = 0
		_FresnelScale("Fresnel Scale", Float) = 1
		_FresnelPower("Fresnel Power", Float) = 1

	}
	SubShader
	{
		Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }

		Pass 
		{
			Lighting Off
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Fog { Mode Off }

			CGPROGRAM

			#pragma vertex vert 
			#pragma fragment frag 

			#include "UnityCG.cginc"

			struct appdata_t
			{
					float4 vertex  : POSITION;
					float4 texcoord : TEXCOORD0;
					half3 normal : NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				half2 uv : TEXCOORD0;
				float fresnel : TEXCOORD1;
			};

			sampler2D _MainTex;
			fixed4 _GhostColor;
			float _Offset;
			float _GhostAlpha;
			float _ShakeLevel;
			float _ShakeSpeed;
			float _Control;
			float4 _ShakeDir;

			fixed4 _FresnelColor;
			fixed _FresnelBias;
			fixed _FresnelScale;
			fixed _FresnelPower;

			v2f vert(appdata_t v)
			{
				float yOffset = 0.5 * (floor(v.vertex.x * 10) % 2);

				v2f o;
				v.vertex += _Offset * cos(_Time.y * _ShakeSpeed) * _ShakeDir * _Control;

				o.vertex = UnityObjectToClipPos(v.vertex);

				float3 i = normalize(ObjSpaceViewDir(v.vertex));
				o.fresnel = _FresnelBias + _FresnelScale * (1 + dot(i, v.normal));

				o.uv = v.texcoord;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv) * _GhostColor;
				col.rgb = lerp(col.rgb, _FresnelColor.rgb, 1 - i.fresnel);
				col.a = _GhostAlpha ;
				return col;
			}

			ENDCG
		}

	}
}