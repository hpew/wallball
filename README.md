# WallBall

### Назначение проекта
<p style="text-align: justify; text-indent: 47px;">Игровой проект, предназначенный для выпуска в Google Store. В игре необходимо разрушать появляющиеся препятствия, чтобы игрок мог набрать как можно больше очков. </p>


### Время на разработку
<p style="text-align: justify; text-indent: 47px;">На разработку проекта давалось 48 часов.</p>

#### [Рабочий билд](https://clck.ru/SkUpv)

#### [Видео геймплея](https://paulradzkov.com/2014/markdown_cheatsheet/)

### Разработчики
* Программист программистов
* Программист программистов
* Программист программистов

gmail - hpewGames@gmail.com.

<img src="./Logo.png" alt="HPEW Logo" width="50"/>

